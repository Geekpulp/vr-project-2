Glen Young
4 hours (estimated)
I enjoyed creating customisations to the apartment and learning a few tips and tricks (vertex snapping) along the way.

The thing I found most challenging was the animation component. Not so much the spinning, that was easy enough, but the on click start stop to the spinning. Because you jump around the scene on click it made it a bit confusing. And I wasn’t completely clear on the state machine stuff. Think I have it nailed now though.